## INFORMATIE

**Studenten**:              Senne Vanfleteren en Lee Van Hecke

**Opleidingsonderdeel**:    NMDAD II

**Academiejaar**:           2015-2016

**Opleiding**:              Bachelor in de grafische en digitale media

**Afstudeerrichting**:      Multimediaproductie

**Keuzeoptie**:             proDEV

**Opleidingsinstelling**:   Arteveldehogeschool


- - - -

## BRIEFING

Seak is een webwinkel die hedendaagse kledij aanbiedt. Het bedrijf is is opgericht in 2015 door Lee Van Hecke en Brent Mouton. Dit zijn twee studenten Grafische en digitale media aan de Arteveldehogeschool. Met het bedrijf willen we comfortkleding met onze eigen designs op de markt brengen. 

Het hoofddoel van de webapplicatie is verkopen van de kledij. Naast de website wordt ook Instagram gebruikt om hun imago te versterken en merkbekendheid te creëren.

De doelgroep die we het meest willen aanspreken zijn jongeren die de comfortkleding als dagelijkse outfit dragen. Zo kunnen zij de trend verspreiden. Uiteraard willen wij ook volwassenen bereiken maar meestal volgen zij de jongerentrends. Seak verstuurd zijn producten voorlopig enkel binnen de Belgische grenzen.

De webapplicatie heeft een vrij grote structuur. Met zijn 33 pagina’s willen wij 3 grote kledingscategorieën aanbieden. T-shirts, Sweatshirts en Hoodies. Dit telkens voor mannen en voor vrouwen. Daarnaast willen we ook nog accessoires aanbieden zoals petten, etc. Het aantal producten waarmee we starten is voorlopig nog onbekend maar uiteindelijk zal het aantal steeds groeien.

De Seak-applicatie kunnen we kort omschrijven als #modern #trendy #hip #comfortable. Deze woorden omschrijven meteen ook de producten die we aanbieden en het imago waarvoor we willen staan. De website moet door zoekmachines gevonden worden op basis van de woorden: T-shirt, T-shirts, Sweatshirts, Sweater, Hoodie, Hoodies, Comfort en Clothing.

Er is heel veel concurrentie voor kledij op de markt. Grote winkelketens zoals H&M, Zara,… zijn tegenwoordig ook online beschikbaar. Dit maakt hen tot de grootste concurrenten. Wij willen ons onderscheiden door uniek te zijn in het merk en onze opschriften. Ook met ons ECO-label willen wij naar buiten komen. Goedkope kledij heeft echter steeds de naam om onder slechte omstandigheden en met slechte stoffen gemaakt te zijn. Hier willen wij niets mee te maken hebben.

- - - -

## DEPLOYEREN

### Klonen

```
$ cd ~/code/
```

```
$ git clone https://sennvanf@bitbucket.org/nmdadiigroep12/seak.local.git
```

### Installeren

```
$ cd ~/code/seak.local/
```

```
$ composer install
```

```
$ cd www
```

```
$ composer install
```

### Aanmaken van Artestead.yaml

```
$ cd ~/code/seak.local/
```

```
$ artestead make --type laravel
```

### Aanmaken van .env a.h.v. .env.example

```
$ cd ~/code/seak.local/www/
```

```
$ cp .env.example .env
```

### Starten van virtuele machine

```
$ cd ~/code/seak.local/
```

```
$ vagrant up
```

```
$ vagrant ssh
```

### Aanmaken database

```
$ cd ~/code/seak.local/www/
```

```
$ composer dump-autoload
```

```
$ artisan artevelde:database:user
```

```
$ artisan artevelde:database:init
```

```
$ artisan migrate --seed
```

### Backoffice bekijken

Ga in je browser naar: http://www.seak.local

### node_modules installeren

(Buiten virtuele machine:)
```
$ cd ~/code/seak.local/www/app/
```

```
$ npm install
```

### Distmap aanmaken en vullen

```
$ gulp
```

### Frontoffice bekijken

```
$ gulp serve:server
```
OF

```
$ gulp serve:browser-sync
```