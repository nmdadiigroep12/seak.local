(gulp => {
    'use strict';

    const CFG = global.CONFIG;

    gulp.task('favicon', () => {
        gulp.src(`${CFG.dir.src}images/**/*.{ico}`)
            .pipe(gulp.dest(`${CFG.dir.dest}..`));
    });

})(require('gulp'));