(gulp => {
    'use strict';

    const CFG = global.CONFIG;

    gulp.task('images', () => {
        gulp.src(`${CFG.dir.src}images/**/*.{gif,ico,jpg,jpeg,png,svg}`)
            .pipe(gulp.dest(`${CFG.dir.dest}images`));
    });

})(require('gulp'));