/**
 * @author    Senne Vanfleteren and Lee Van Hecke (based on code by Olivier Parent)
 * @copyright Copyright © 2015-2016 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () {
    'use strict';

    // Module declarations
    angular.module('app', [
        // Angular Module Dependencies
        // ---------------------------
        'ngAnimate',
        'ngMaterial',
        'ngMessages',
        'ngResource',

        // Third-party Module Dependencies
        // -------------------------------
        'ui.router', // Angular UI Router: https://github.com/angular-ui/ui-router/wiki

        // Custom Module Dependencies
        // --------------------------
        'app.mannen',
        'app.vrouwen',
        'app.accessoires',
        'app.kledij',
        'app.home',
        'app.services',
        'app.login',
        'app.over-ons',
        'app.ecologie',
        'app.contact',
        'app.mijn-account',
        'app.winkelmandje',
        'app.verlanglijst',
        'app.toegang',
        'app.bestellingen',
        'app.persoonlijk'
    ]);
    angular.module('app.mannen', []);
    angular.module('app.vrouwen', []);
    angular.module('app.accessoires', []);
    angular.module('app.kledij', []);
    angular.module('app.home', []);
    angular.module('app.services', []);
    angular.module('app.login', []);
    angular.module('app.ecologie', []);
    angular.module('app.over-ons', []);
    angular.module('app.contact', []);
    angular.module('app.mijn-account', []);
    angular.module('app.winkelmandje', []);
    angular.module('app.verlanglijst', []);
    angular.module('app.toegang', []);
    angular.module('app.bestellingen', []);
    angular.module('app.persoonlijk', []);

    // Make wrapper services and remove globals for Third-party Libraries
    angular.module('app')
        .run(Run);

    /* @ngInject */
    function Run(
        $_,
        $faker
    ) {}
})();