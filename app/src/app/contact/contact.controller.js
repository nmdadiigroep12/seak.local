/**
 * @author    Senne Vanfleteren and Lee Van Hecke (based on code by Olivier Parent)
 * @copyright Copyright © 2015-2016 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () {
    'use strict';

    angular.module('app.contact')
        .controller('ContactController', ContactController);

    /* @ngInject */
    function ContactController(
        // Angular
        $log,
        $scope,
        // Third-party
        $_,
        $faker
    ) {
        $log.log('Gebruikte controller:', ContactController.name);

        // ViewModel
        // =========
        var vm = this;

        // Interaction
        // -----------
        vm.$$ix = {};

        // User Interface
        // --------------
        vm.$$ui = {
            title: 'Contact'
        };
    }

})();
