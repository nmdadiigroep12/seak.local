/**
 * @author    Senne Vanfleteren and Lee Van Hecke (based on code by Olivier Parent)
 * @copyright Copyright © 2015-2016 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () {
    'use strict';

    angular.module('app.contact')
        .config(Routes);

    /* @ngInject */
    function Routes(
        // Angular
        $stateProvider
    ) {
        $stateProvider
            .state('contact', {
                cache: false, // false will reload on every visit.
                controller: 'ContactController as vm',
                templateUrl: 'html/contact/contact.view.html',
                url: '/contact'
            });
    }

})();