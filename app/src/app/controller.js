/**
 * @author    Senne Vanfleteren and Lee Van Hecke (based on code by Olivier Parent)
 * @copyright Copyright © 2015-2016 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () {
    'use strict';

    angular.module('app')
        .controller('AppCtrl', AppCtrl)
        .controller('LeftCtrl', LeftCtrl)
        .controller('RightCtrl', RightCtrl);

    function AppCtrl(
        $scope,
        $mdSidenav,
        $log,
        login,
        $state
    ) {
        // ViewModel
        // =========
        var app = this;

        // User Interface
        // --------------

        $log.log('Gebruikte controller:', AppCtrl.name);

        $scope.$watch(login.authenticated, function() {
            if( login.authenticated()){
                app.$$ui = {
                    menuapp: [
                        { label: 'SEAK'       , uri: '#'       , state: 'home'  },
                        { label: 'Mannen'       , uri: '#/mannen'       , state: 'kledij.mannen'  },
                        { label: 'Vrouwen'       , uri: '#/vrouwen'       , state: 'kledij.vrouwen'  },
                        { label: 'Accessoires'       , uri: '#/accessoires'       , state: 'kledij.accessoires'  },
                        { label: 'Ecologie'       , uri: '#/ecologie'       , state: 'ecologie'  }
                    ],
                    menuuser: [
                        { label: 'Mijn account', uri: '#/mijn-account', state: 'mijn-account.mijnaccount' },
                        { label: 'Winkelmandje', uri: '#/winkelmandje', state: 'winkelmandje.items' },
                        { label: 'Contact', uri: '#/contact', state: 'contact' },
                        { label: 'Over ons', uri: '#/over-ons', state: 'over-ons' },
                        { label: 'Uitloggen', uri: '#/log-in', state: 'login.uitloggen' }
                    ],
                    title: 'Seak',
                    user: login.user
                };
                $log.log('Aangemelde gebruiker:', login.user());
            }
            else {
                app.$$ui = {
                    menuapp: [
                        { label: 'SEAK'       , uri: '#'       , state: 'home'  },
                        { label: 'Mannen'       , uri: '#/mannen'       , state: 'kledij.mannen'  },
                        { label: 'Vrouwen'       , uri: '#/vrouwen'       , state: 'kledij.vrouwen'  },
                        { label: 'Accessoires'       , uri: '#/accessoires'       , state: 'kledij.accessoires'  },
                        { label: 'Ecologie'       , uri: '#/ecologie'       , state: 'ecologie'  }
                    ],
                    menuuser: [
                        { label: 'Log in / Registreer', uri: '#/log-in', state: 'login.inloggen' },
                        { label: 'Winkelmandje', uri: '#/winkelmandje', state: 'winkelmandje.items' },
                        { label: 'Contact', uri: '#/contact', state: 'contact' },
                        { label: 'Over ons', uri: '#/over-ons', state: 'over-ons' }
                    ],
                    title: 'Seak'
                };
            }
        })



        $log.log('Aangemelde gebruiker:', login.user());

        $scope.isLeftSidenavOpen = false;
        $scope.isRightSidenavOpen = false;

        $scope.toggleLeft = buildToggler('left');
        $scope.toggleRight = buildToggler('right');
        function buildToggler(navID) {
            return function() {
                $mdSidenav(navID)
                    .toggle()
            }
        }
        $scope.$watch('isLeftSidenavOpen', function(isLeftSidenavOpen) {
            $("body").css({"overflow": (isLeftSidenavOpen ? "hidden" : "scroll")});
        });
        $scope.$watch('isRightSidenavOpen', function(isRightSidenavOpen) {
            $("body").css({"overflow": (isRightSidenavOpen ? "hidden" : "scroll")});
        });
    }
    function LeftCtrl(
        $scope,
        $mdSidenav
    ) {
        $scope.close = function () {
            $mdSidenav('left').close()
        };
    }
    function RightCtrl(
        $scope,
        $mdSidenav
    ) {
        $scope.close = function () {
            $mdSidenav('right').close()
        };
    }



})();