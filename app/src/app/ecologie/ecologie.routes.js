/**
 * @author    Senne Vanfleteren and Lee Van Hecke (based on code by Olivier Parent)
 * @copyright Copyright © 2015-2016 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () {
    'use strict';

    angular.module('app.ecologie')
        .config(Routes);

    /* @ngInject */
    function Routes(
        // Angular
        $stateProvider
    ) {
        $stateProvider
            .state('ecologie', {
                cache: false, // false will reload on every visit.
                controller: 'EcologieController as vm',
                templateUrl: 'html/ecologie/ecologie.view.html',
                url: '/ecologie'
            });
    }

})();