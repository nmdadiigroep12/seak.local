/**
 * @author    Senne Vanfleteren and Lee Van Hecke (based on code by Olivier Parent)
 * @copyright Copyright © 2015-2016 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () {
    'use strict';

    // Module declarations
    angular.module('app').factory('kledij', kledij);

    kledij.$inject = [
        // Angular
        '$http',
        '$httpParamSerializerJQLike',

    ];

    function kledij($http) {

        /*------------------------------------------*\
         Properties
         \*------------------------------------------*/


        /*------------------------------------------*\
         Factory
         \*------------------------------------------*/
        return {

            category : function(id){
                return $http.get('http://www.seak.local/api/v1/category/' + id);
            },

            product : function(id){
                return $http.get('http://www.seak.local/api/v1/product/' + id);
            }
        }

    }

})();
