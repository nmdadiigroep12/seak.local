/**
 * @author    Senne Vanfleteren and Lee Van Hecke (based on code by Olivier Parent)
 * @copyright Copyright © 2015-2016 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () {
    'use strict';

    // Module declarations
    angular.module('app').factory('login', login);

    function login(
        $http,
        $httpParamSerializerJQLike,
        $state
    ) {

        /*------------------------------------------*\
                          Properties
        \*------------------------------------------*/
        var authenticated = false;
        var user = null;

        /*------------------------------------------*\
                           Factory
        \*------------------------------------------*/
        return {

            inloggen: function(email, password){

                /**
                 * Object to send to laravel
                 */
                var request = {
                    email: email,
                    password: password
                };

                /**
                 * Post the object to laravel
                 */
                var response = $http({
                    method: 'POST',
                    url: 'http://www.seak.local/api/v1/login',
                    headers: { 'Content-Type' : 'application/x-www-form-urlencoded' },
                    data: $httpParamSerializerJQLike(request)
                })
                .success(function(login){
                    /**
                     * We get an object back
                     *
                     * Valid: true if credentials match
                     * User: the user if valid
                     */
                    if(login.valid){
                        authenticated = true;
                        user = login.user;
                        $state.go('mijn-account.mijnaccount', {}, {reload: true});
                    }
                })
                .error(function(){
                    console.log('Het aanmelden is mislukt, gelieve opnieuw te proberen.');
                });

                return response;
            },

            registreren: function(email, password, naam, voornaam, adres, postcode, stad){

                /**
                 * Object to send to laravel
                 */
                var request = {
                    email: email,
                    password: password,
                    naam: naam,
                    voornaam: voornaam,
                    adres: adres,
                    postcode: postcode,
                    stad: stad
                };

                /**
                 * Post the object to laravel
                 */
                var response = $http({
                    method: 'POST',
                    url: 'http://www.seak.local/api/v1/login/registreren',
                    headers: { 'Content-Type' : 'application/x-www-form-urlencoded' },
                    data: $httpParamSerializerJQLike(request)
                })
                .success(function(login){
                    /**
                     * We get an object back
                     *
                     * Valid: true if credentials match
                     * User: the user if valid
                     */
                    if(login.success){
                        authenticated = true;
                        user = login.user;
                        $state.go('mijn-account.mijnaccount');
                    }
                })
                .error(function(){
                    console.log('error!');
                });

                return response;
            },

            uitloggen: function(){
                authenticated = false;
                user = null;
                $state.go('home');
            },

            delete: function(){
                authenticated = false;
                user = null;
                $state.go('home');
            },

            authenticated: function(){
                return authenticated;
            },

            user: function(){
                return user;
            }
        }

    }

})();
