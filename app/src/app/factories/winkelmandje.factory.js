/**
 * @author    Senne Vanfleteren and Lee Van Hecke (based on code by Olivier Parent)
 * @copyright Copyright © 2015-2016 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () {
    'use strict';

    // Module declarations
    angular.module('app').factory('kledij', kledij);

    function kledij($http, $httpParamSerializerJQLike, login) {

        /*------------------------------------------*\
                         Properties
        \*------------------------------------------*/
        var winkelmandje = {
            count : 0,
            items: [],
            add: function(product){
                winkelmandje.items.push(product);
                product.inwinkelmandje = true;
            },
            remove: function(product){
                var index = winkelmandje.items.indexOf(product);
                if(index >= 0)winkelmandje.items.splice(index, 1);
            },
            total: function(){},
            checkout: function(){
                /**
                 * Object to send to laravel
                 */
                var request = {
                    customer_id : login.user().id,
                    items : winkelmandje.items
                };

                if(winkelmandje.items.length > 0){
                    var response = $http({
                        method: 'POST',
                        url: 'http://www.seak.local/api/v1/checkout',
                        headers: { 'Content-Type' : 'application/x-www-form-urlencoded' },
                        data: $httpParamSerializerJQLike(request)
                    })
                    .success(function(data){
                        console.log(data);
                        winkelmandje.items = [];
                    })
                    .error(function(){
                        console.log('Het is niet gelukt om de bestelling te plaatsen.');
                    });
                }
            },
        }


        /*------------------------------------------*\
                           Factory
        \*------------------------------------------*/
        return {
            // get sales
            sales : function() {
                return $http.get('http://www.seak.local/api/v1/sales');
            },

            // get popular
            popular : function() {
                return $http.get('http://www.seak.local/api/v1/popular');
            },

            // get recent
            recent : function() {
                return $http.get('http://www.seak.local/api/v1/recent');
            },

            category : function(id){
                return $http.get('http://www.seak.local/api/v1/category/' + id);
            },

            product : function(id){
                return $http.get('http://www.seak.local/api/v1/product/' + id);
            },


            winkelmandje : function(){
                return winkelmandje;
            },


        }

    }

})();
