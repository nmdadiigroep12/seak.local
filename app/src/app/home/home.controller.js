/**
 * @author    Senne Vanfleteren and Lee Van Hecke (based on code by Olivier Parent)
 * @copyright Copyright © 2015-2016 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () {
    'use strict';

    angular.module('app.home', ['ngMaterial'])
        .controller('HomeController', HomeController)

    /* @ngInject */
    function HomeController(
        // Angular
        $log,
        login
    ) {
        $log.log('Gebruikte controller:', HomeController.name);

        // ViewModel
        // =========
        var vm = this;

        // Interaction
        // -----------
        vm.$$ix = {};

        // User Interface
        // --------------
        vm.$$ui = {
            title: 'Home'
        }
    }

})();
