/**
 * @author    Senne Vanfleteren and Lee Van Hecke (based on code by Olivier Parent)
 * @copyright Copyright © 2015-2016 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () {
    'use strict';

    angular.module('app.mannen')
        .controller('MannenController', MannenController);

    // Inject dependencies into constructor (needed when JS minification is applied).
    MannenController.$inject = [
        // Angular
        '$log',
        '$scope',
        '$http',
        // Custom
        'kledij'
    ];

    function MannenController(
        // Angular
        $log,
        $scope,
        $http,
        // Custom
        kledij
    ) {
        // ViewModel
        // =========
        var vm = this;

        vm.$$ui = {
            title: 'Mannen'
        };

        vm.mannen = null;

        kledij.category(1)
            .success(function(category) {
                vm.mannen = category.products;
            });
    }

    angular.module('app.vrouwen')
        .controller('VrouwenController', VrouwenController);

    // Inject dependencies into constructor (needed when JS minification is applied).
    VrouwenController.$inject = [
        // Angular
        '$log',
        '$scope',
        '$http',
        // Custom
        'kledij'
    ];

    function VrouwenController(
        // Angular
        $log,
        $scope,
        $http,
        // Custom
        kledij
    ) {
        // ViewModel
        // =========
        var vm = this;

        vm.$$ui = {
            title: 'Vrouwen'
        }

        vm.vrouwen = null;

        kledij.category(2)
            .success(function(category) {
                vm.vrouwen = category.products;
            });
    }

    angular.module('app.accessoires')
        .controller('AccessoiresController', AccessoiresController);

    // Inject dependencies into constructor (needed when JS minification is applied).
    AccessoiresController.$inject = [
        // Angular
        '$log',
        '$scope',
        '$http',
        // Custom
        'kledij'
    ];

    function AccessoiresController(
        // Angular
        $log,
        $scope,
        $http,
        // Custom
        kledij
    ) {
        // ViewModel
        // =========
        var vm = this;

        vm.$$ui = {
            title: 'Accessoires'
        }

        vm.accessoires = null;

        kledij.category(3)
            .success(function(category) {
                vm.accessoires = category.products;
            });
    }

})();