/**
 * @author    Senne Vanfleteren and Lee Van Hecke (based on code by Olivier Parent)
 * @copyright Copyright © 2015-2016 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () {
    'use strict';

    angular.module('app.kledij')
        .config(Routes);

    function Routes(
        // Angular
        $stateProvider
    ) {
        $stateProvider
            .state('kledij', {
                cache: false, // false will reload on every visit.
                templateUrl: 'html/kledij/kledij.view.html',
                url: '/kledij'
            })
            .state('kledij.mannen', {
                cache: false, // false will reload on every visit.
                controller: 'MannenController as vm',
                templateUrl: 'html/kledij/kledij-mannen.partial.html',
                url: '/mannen'
            })
            .state('kledij.vrouwen', {
                cache: false, // false will reload on every visit.
                controller: 'VrouwenController as vm',
                templateUrl: 'html/kledij/kledij-vrouwen.partial.html',
                url: '/vrouwen'
            })
            .state('kledij.accessoires', {
                cache: false, // false will reload on every visit.
                controller: 'AccessoiresController as vm',
                templateUrl: 'html/kledij/kledij-accessoires.partial.html',
                url: '/accessoires'
            })
            .state('kledij.product', {
                cache: false, // false will reload on every visit.
                controller: 'ProductController as vm',
                templateUrl: 'html/kledij/kledij-product.partial.html',
            })
            .state('kledij.product.show', {
                cache: false, // false will reload on every visit.
                templateUrl: 'html/kledij/product.partial.html',
                url: '/product/{product_id:int}'
            });
        ;
    }

})();
