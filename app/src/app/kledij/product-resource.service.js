/**
 * @author    Senne Vanfleteren and Lee Van Hecke (based on code by Olivier Parent)
 * @copyright Copyright © 2015-2016 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () {
    'use strict';

    angular.module('app.kledij')
        .factory('ProductResource', ProductResource);

    /* @ngInject */
    function ProductResource(
        // Angular
        $resource,
        // Custom
        UriService
    ) {
        var url = UriService.getApi('products/:product_id')

        var paramDefaults = {
            product_id: '@id'
        };

        var actions = {
            'update': {
                method: 'PUT'
            }
        };

        return $resource(url, paramDefaults, actions);
    }

})();
