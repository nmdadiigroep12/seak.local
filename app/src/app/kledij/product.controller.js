/**
 * @author    Senne Vanfleteren and Lee Van Hecke (based on code by Olivier Parent)
 * @copyright Copyright © 2015-2016 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () {
    'use strict';

    angular.module('app.kledij')
        .controller('ProductController', ProductController);

    /* @ngInject */
    function ProductController(
        // Angular
        $cacheFactory,
        $log,
        // Third-party Modules
        $state,
        // Custom
        SizeResource,
        ProductResource,
        UriService
    ) {
        $log.info('Gebruikte controller:', ProductController.name);
        $log.log('Current state name:', $state.current.name);

        // ViewModel
        // =========
        var vm = this;

        // User Interface
        // --------------
        vm.$$ui = {};

        // Data
        // ----
        vm.product = new ProductResource();

        // States
        // ------
        switch ($state.current.name) {
            case 'kledij.product.show':
                vm.product = getProductAction();
                vm.$$ui.sizes = SizeResource.query();
                break;
            default:
                break;
        }

        // Functions
        // =========

        $('#heart').click(function(){
            if($(this).find($(".fa")).hasClass('fa-heart-o')){
                $(this).find($(".fa-heart-o")).removeClass('fa-heart-o').addClass('fa-heart')
            }
            else if($(this).find($(".fa")).hasClass('fa-heart')){
                $(this).find($(".fa-heart")).removeClass('fa-heart').addClass('fa-heart-o');
            }
        });

        // Products
        // -----

        function getProductAction() {

            var params = {
                product_id: $state.params.product_id
            };

            return ProductResource
                .get(params, success, error);

            function error(error) {
                $log.error(getProductAction.name, 'ERROR', 'error:', error);
            }

            function success(resource) {
                $log.log(resource);
                vm.product = resource;
                vm.$$ui.loader = true;
            }

        }
    }

})();
