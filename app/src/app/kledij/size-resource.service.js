/**
 * @author    Senne Vanfleteren and Lee Van Hecke (based on code by Olivier Parent)
 * @copyright Copyright © 2015-2016 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () {
    'use strict';

    angular.module('app.accessoires')
        .factory('SizeResource', SizeResource);

    /* @ngInject */
    function SizeResource(
        // Angular
        $resource,
        // Custom
        UriService
    ) {
        var url = UriService.getApi('sizes/:size_id');

        var paramDefaults = {
            size_id : '@id'
        };

        var actions = {};

        return $resource(url, paramDefaults, actions);
    }

})();
