/**
 * @author    Senne Vanfleteren and Lee Van Hecke (based on code by Olivier Parent)
 * @copyright Copyright © 2015-2016 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () {
    'use strict';

    angular.module('app.login')
        .controller('loginController', loginController);

    function loginController(
        $log,
        $scope,
        $http,
        $state,
        login
    ) {
        $log.log('Gebruikte controller:', loginController.name);
        $log.log('Huidige staat:', $state.current.name);

        // ViewModel
        // =========
        var vm = this;

        // User Interface
        // --------------
        vm.$$ui = {
            title: 'Log in / Registreer'
        };

        // Data
        // ----
        vm.inloggen = {
            email : null,
            password : null,
            submit : function(){
                if(this.email == null || this.password == null || this.email == 'undefined' || this.password == 'undefined'){
                    alert('Vul uw email en wachtwoord in of klik op registreren');
                }
                else{
                    login.inloggen(this.email, this.password)
                        .success(function(login) {
                            if(!login.valid){
                                alert('Het e-mailadres of wachtwoord is onjuist. Gelieve opnieuw te proberen of een account aan te maken.');
                            }
                        });
                }
            }
        };

        vm.registreren = {
            email : null,
            password : null,
            naam : null,
            voornaam : null,
            adres : null,
            postcode : null,
            stad : null,
            submit : function(){
                if(
                    this.email == null || this.email == 'undefined' ||
                    this.password == null || this.password == 'undefined' ||
                    this.naam == null || this.naam == 'undefined'||
                    this.voornaam == null || this.voornaam == 'undefined'||
                    this.adres == null || this.adres == 'undefined'||
                    this.postcode == null || this.postcode == 'undefined'||
                    this.stad == null || this.stad == 'undefined'
                ){
                    console.log('Gelieve alle velden in te vullen!');
                }
                else
                {
                    login.registreren(this.email, this.password, this.naam, this.voornaam, this.adres, this.postcode, this.stad)
                        .success(function(login) {
                            if(!login.success){
                                console.log(login.error);
                            }
                        });
                }
            }
        };

        vm.uitloggen = {
            submit : function(){
                login.uitloggen()
            }
        };

        vm.delete = {
            submit : function(){
                login.delete()
            }
        };
    }

})();
