/**
 * @author    Senne Vanfleteren and Lee Van Hecke (based on code by Olivier Parent)
 * @copyright Copyright © 2015-2016 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () {
    'use strict';

    angular.module('app.login')
        .config(Routes);

    // Inject dependencies into constructor (needed when JS minification is applied).
    Routes.$inject = [
        // Angular
        '$stateProvider'
    ];

    /* @ngInject */
    function Routes(
        // Angular
        $stateProvider
    ) {
        $stateProvider
            .state('login', {
                cache: false, // false will reload on every visit.
                controller: 'loginController as vm',
                templateUrl: 'html/login/login.view.html',
                url: '/login'
            })
            .state('login.inloggen', {
                cache: false, // false will reload on every visit.
                templateUrl: 'html/login/login-inloggen.partial.html',
                url: '/inloggen'
            })
            .state('login.registreren', {
                cache: false, // false will reload on every visit.
                templateUrl: 'html/login/login-registreren.partial.html',
                url: '/registreren'
            })
            .state('login.uitloggen', {
                cache: false, // false will reload on every visit.
                templateUrl: 'html/login/login-uitloggen.partial.html',
                url: '/uitloggen'
            });
    }

})();