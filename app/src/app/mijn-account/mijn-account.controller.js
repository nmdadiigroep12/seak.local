/**
 * @author    Senne Vanfleteren and Lee Van Hecke (based on code by Olivier Parent)
 * @copyright Copyright © 2015-2016 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () {
    'use strict';

    angular.module('app.mijn-account')
        .controller('MijnAccountController', MijnAccountController);

    /* @ngInject */
    function MijnAccountController(
        // Angular
        $log,
        $scope,
        // Third-party
        $_,
        $http,
        $faker,
        $state,
        login
    ) {
        $log.log('Gebruikte controller:', MijnAccountController.name);

        if( login.authenticated() == false ){
            $state.go('login.inloggen');
        }

        // ViewModel
        // =========
        var vm = this;

        // Interaction
        // -----------
        vm.$$ix = {};

        // User Interface
        // --------------
        vm.$$ui = {
            title: 'Mijn account'
        }


    }

    angular.module('app.persoonlijk')
        .controller('PersoonlijkController', PersoonlijkController);

    /* @ngInject */
    function PersoonlijkController(
        // Angular
        $log,
        $scope,
        // Third-party
        $_,
        $faker,
        login,
        $state
    ) {
        $log.log('Gebruikte controller:', PersoonlijkController.name);

        if( login.authenticated() == false ){
            $state.go('login.inloggen');
        }

        // ViewModel
        // =========
        var vm = this;

        // Interaction
        // -----------
        vm.$$ix = {};

        // User Interface
        // --------------
        vm.$$ui = {
            title: 'Persoonlijk',
            user: login.user
        };
    }

    angular.module('app.toegang')
        .controller('ToegangController', ToegangController);

    /* @ngInject */
    function ToegangController(
        // Angular
        $log,
        login,
        $state,
        $scope,
        // Third-party
        $_,
        $faker
    ) {
        $log.log('Gebruikte controller:', ToegangController.name);

        if( login.authenticated() == false ){
            $state.go('login.inloggen');
        }

        // ViewModel
        // =========
        var vm = this;

        // Interaction
        // -----------
        vm.$$ix = {};

        // User Interface
        // --------------
        vm.$$ui = {
            title: 'Toegang'
        };
    }

    angular.module('app.verlanglijst')
        .controller('VerlanglijstController', VerlanglijstController);

    /* @ngInject */
    function VerlanglijstController(
        // Angular
        $log,
        login,
        $state,
        $scope,
        // Third-party
        $_,
        $faker
    ) {
        $log.log('Gebruikte controller:', VerlanglijstController.name);

        if( login.authenticated() == false ){
            $state.go('login.inloggen');
        }

        // ViewModel
        // =========
        var vm = this;

        // Interaction
        // -----------
        vm.$$ix = {};

        // User Interface
        // --------------
        vm.$$ui = {
            title: 'Verlanglijst'
        };
    }

    angular.module('app.bestellingen')
        .controller('BestellingenController', BestellingenController);

    /* @ngInject */
    function BestellingenController(
        // Angular
        $log,
        login,
        $state,
        $scope,
        // Third-party
        $_,
        $faker
    ) {
        $log.log('Gebruikte controller:', BestellingenController.name);

        if( login.authenticated() == false ){
            $state.go('login.inloggen');
        }

        // ViewModel
        // =========
        var vm = this;

        // Interaction
        // -----------
        vm.$$ix = {};

        // User Interface
        // --------------
        vm.$$ui = {
            title: 'Bestellingen'
        };
    }

})();
