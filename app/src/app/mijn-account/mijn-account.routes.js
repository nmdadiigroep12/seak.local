/**
 * @author    Senne Vanfleteren and Lee Van Hecke (based on code by Olivier Parent)
 * @copyright Copyright © 2015-2016 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () {
    'use strict';

    angular.module('app.mijn-account')
        .config(Routes);

    /* @ngInject */
    function Routes(
        // Angular
        $stateProvider
    ) {
        $stateProvider
            .state('mijn-account', {
                cache: false, // false will reload on every visit.
                controller: 'loginController as vm',
                templateUrl: 'html/mijn-account/mijn-account.view.html',
                url: '/mijn-account'
            })
            .state('mijn-account.mijnaccount', {
                cache: false, // false will reload on every visit.
                controller: 'MijnAccountController as vm',
                templateUrl: 'html/mijn-account/mijn-account-mijnaccount.partial.html',
                url: '/mijnaccount'
            })
            .state('mijn-account.persoonlijk', {
                cache: false, // false will reload on every visit.
                controller: 'PersoonlijkController as vm',
                templateUrl: 'html/mijn-account/mijn-account-persoonlijk.partial.html',
                url: '/persoonlijk'
            })
            .state('mijn-account.toegang', {
                cache: false, // false will reload on every visit.
                controller: 'ToegangController as vm',
                templateUrl: 'html/mijn-account/mijn-account-toegang.partial.html',
                url: '/toegang'
            })
            .state('mijn-account.verlanglijst', {
                cache: false, // false will reload on every visit.
                controller: 'VerlanglijstController as vm',
                templateUrl: 'html/mijn-account/mijn-account-verlanglijst.partial.html',
                url: '/verlanglijst'
            })
            .state('mijn-account.bestellingen', {
                cache: false, // false will reload on every visit.
                controller: 'BestellingenController as vm',
                templateUrl: 'html/mijn-account/mijn-account-bestellingen.partial.html',
                url: '/bestellingen'
            })
            .state('mijn-account.delete', {
            cache: false, // false will reload on every visit.
            controller: 'loginController as vm',
            templateUrl: 'html/mijn-account/mijn-account-delete.partial.html',
            url: '/delete'
        });;
    }

})();