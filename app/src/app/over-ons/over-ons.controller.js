/**
 * @author    Senne Vanfleteren and Lee Van Hecke (based on code by Olivier Parent)
 * @copyright Copyright © 2015-2016 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () {
    'use strict';

    angular.module('app.over-ons')
        .controller('OverOnsController', OverOnsController);

    /* @ngInject */
    function OverOnsController(
        // Angular
        $log,
        $scope,
        // Third-party
        $_,
        $faker
    ) {
        $log.log('Gebruikte controller:', OverOnsController.name);

        // ViewModel
        // =========
        var vm = this;

        // Interaction
        // -----------
        vm.$$ix = {};

        // User Interface
        // --------------
        vm.$$ui = {
            title: 'Over ons'
        };
    }

})();
