/**
 * @author    Senne Vanfleteren and Lee Van Hecke (based on code by Olivier Parent)
 * @copyright Copyright © 2015-2016 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () {
    'use strict';

    angular.module('app.over-ons')
        .config(Routes);

    /* @ngInject */
    function Routes(
        // Angular
        $stateProvider
    ) {
        $stateProvider
            .state('over-ons', {
                cache: false, // false will reload on every visit.
                controller: 'OverOnsController as vm',
                templateUrl: 'html/over-ons/over-ons.view.html',
                url: '/over-ons'
            });
    }

})();