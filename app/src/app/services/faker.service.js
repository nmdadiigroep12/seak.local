/**
 * @author    Olivier Parent
 * @copyright Copyright © 2015-2016 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () {
    'use strict';

    angular.module('app.services')
        .factory('$faker', faker);

    /* @ngInject */
    function faker(
        // Angular
        $window
    ) {
        var faker = $window.faker;

        delete window.faker;
        delete $window.faker;

        return faker;
    }

})();
