/**
 * @author    Senne Vanfleteren and Lee Van Hecke (based on code by Olivier Parent)
 * @copyright Copyright © 2015-2016 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () {
    'use strict';

    angular.module('app.winkelmandje')
        .controller('WinkelmandjeController', WinkelmandjeController)
        .controller('CheckOutController', CheckOutController);

    /* @ngInject */
    function WinkelmandjeController(
        // Angular
        $log,
        login,
        $scope,
        // Third-party
        $_,
        $faker
    ) {
        $log.log('Gebruikte controller:', WinkelmandjeController.name);

        // ViewModel
        // =========
        var vm = this;

        // Interaction
        // -----------
        vm.$$ix = {};

        // User Interface
        // --------------
        vm.$$ui = {
            title: 'Winkelmandje'
        };
    }

    /* @ngInject */
    function CheckOutController(
        // Angular
        $log,
        login,
        $scope,
        $state,
        // Third-party
        $_,
        $faker
    ) {
        $log.log('Gebruikte controller:', CheckOutController.name);

        if( login.authenticated() == false ){
            $state.go('login.inloggen');
            alert("Gelieve in te loggen of een account aan te maken om uw bestelling te kunnen plaatsen.");
        }

        // ViewModel
        // =========
        var vm = this;

        // Interaction
        // -----------
        vm.$$ix = {};

        // User Interface
        // --------------
        vm.$$ui = {
            title: 'Check out'
        };
    }

})();
