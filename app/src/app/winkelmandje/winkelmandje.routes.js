/**
 * @author    Senne Vanfleteren and Lee Van Hecke (based on code by Olivier Parent)
 * @copyright Copyright © 2015-2016 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () {
    'use strict';

    angular.module('app.winkelmandje')
        .config(Routes);

    /* @ngInject */
    function Routes(
        // Angular
        $stateProvider
    ) {
        $stateProvider
            .state('winkelmandje', {
                cache: false, // false will reload on every visit.
                controller: 'WinkelmandjeController as vm',
                templateUrl: 'html/winkelmandje/winkelmandje.view.html',
                url: '/winkelmandje'
            })
            .state('winkelmandje.items', {
                cache: false, // false will reload on every visit.
                templateUrl: 'html/winkelmandje/winkelmandje-items.partial.html',
                url: '/items'
            })
            .state('winkelmandje.checkout', {
                cache: false, // false will reload on every visit.
                controller: 'CheckOutController as vm',
                templateUrl: 'html/winkelmandje/winkelmandje-checkout.partial.html',
                url: '/checkout'
            })
            .state('winkelmandje.bedankt', {
                cache: false, // false will reload on every visit.
                controller: 'CheckOutController as vm',
                templateUrl: 'html/winkelmandje/winkelmandje-bedankt.partial.html',
                url: '/bedankt'
            })
    }

})();