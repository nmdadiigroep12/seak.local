<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Category;

class CategoriesController extends Controller
{
    /**
     * Get the products from a category
     *
     * @return JSON
     */
    public function categories()
    {
        /**
         * Find the refered category with it's products
         */
        $categories = Category::with('products')->get();

        return $categories->toArray();
    }

    /**
     * Get the products from a category
     *
     * @return JSON
     */
    public function category($id)
    {
        /**
         * Find the refered category with it's products
         */
        $category = Category::with('products')->findOrFail($id);

        return $category->toArray();
    }
}
