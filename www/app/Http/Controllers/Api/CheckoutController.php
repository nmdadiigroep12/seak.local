<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Product;
use App\Models\Order;

class CheckoutController extends Controller
{
    public function checkout(Request $request)
    {
        /**
         * Get the data from angular
         */
        $input = $request->all();

        $id = $input['customer_id'];

        /**
         * Create a purchase for each item in the cart
         */
        foreach ($input['items'] as $key => $item) {
            /**
             * Get the product
             */
            $product = Product::findOrFail($item['id']);

            $price = $product->price;

            Order::create([
                'user_id'   => $id,
                'title'     => $product->title,
                'content'   => $product->content,
                'image_url' => $product->image_url,
                'price'     => $price
            ]);
        }

        return array(
            "success" => true
        );
    }
}
