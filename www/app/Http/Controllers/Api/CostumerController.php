<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\User;
use Hash;

class CostumerController extends Controller
{
    public function login(Request $request)
    {
        /**
         * Get the data from angular
         */
        $input = $request->all();
        $email = $input['email'];
        $password = $input['password'];

        /**
         * Get the user
         */
        $user = User::where('email', $email)->first();

        /**
         * If email is not found
         */
        if(!isset($user) || $user == null){
            return array(
                "valid" => false,
            );
        }

        return array(
            "user"  => $user,
            "valid" => Hash::check($password, $user->password),
        );
    }

    /**
     * Register a new user
     *
     * @return JSON
     */
    public function registreren(Request $request){
        /**
         * Get the data from angular
         */
        $input = $request->all();
        $voornaam = $input['voornaam'];
        $naam = $input['naam'];
        $email = $input['email'];
        $password = $input['password'];
        $adres = $input['adres'];
        $postcode = $input['postcode'];
        $stad = $input['stad'];

        $exists = User::where('email', $email)->first();

        if (isset($exists)){
            return array(
                "user"  => null,
                "success" => false,
                "error" => 'Email is already in use',
            );
        }

        /**
         * Create the user
         */
        $user = User::create([
            'email' => $email,
            'voornaam' => $voornaam,
            'naam' => $naam,
            'password' => Hash::make($password),
            'adres' => $adres,
            'postcode' => $postcode,
            'stad' => $stad,
        ]);

        /**
         * If the user creation was succesful
         */
        if(isset($user) && $user != null){
            $response = array(
                "user"  => $user,
                "success" => true,
            );
        }
        else{
            $response = array(
                "user"  => null,
                "success" => false,
                "error" => 'User could not be created',
            );
        }

        return $response;

    }
}
