<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Product;
use App\User;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class ProductsController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function show($id)
    {
        $product = Product::with('category')->find($id);

        return $product ?: response()
            ->json([
                'error' => "Product `${id}` not found",
            ])
            ->setStatusCode(Response::HTTP_NOT_FOUND);
    }

    public function product($id)
    {
        /**
         * Find the refered product
         */
        $product = product::findOrFail($id);

        return $product;
    }
}
