<?php

namespace App\Http\Controllers\Coupon;

use Illuminate\Http\Request;
use App\Models\Coupon;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\View\View;

class CouponController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() : View
    {
        $coupons = Coupon::with('user')->orderBy('created_at', 'DESC')->get(); // Eager Loading of related Category, Tag, and User objects.

        \Debugbar::info($coupons);

        $data = [
            'coupons' => $coupons,
        ];

        return view('coupons.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() : View
    {
        $coupons = new Coupon();

        $data = [
            'coupons' => $coupons,
        ];

        return view('coupons.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'title' => 'required|unique:products|max:255',
            'description' => 'required',
        ];

        $this->validate($request, $rules);

        $coupons = new Coupon($request->all());
        $coupons->user()->associate($request->user());
        $coupons->save();

        return redirect()->route('coupon.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) : View
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) : View
    {
        $coupons = Coupon::find($id);

        $data = [
            'coupons' => $coupons,
        ];

        return view('coupons.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = [
            'title' => 'required|max:255',
            'description' => 'required',
            'code' => 'required|int',
        ];

        $this->validate($request, $rules);

        $coupons = Coupon::find($id);

        $coupons->title = $request->get('title');
        $coupons->description = $request->get('description');
        $coupons->percentage = $request->get('percentage');
        $coupons->expire = $request->get('expire');
        $coupons->save();

        return redirect()->route('coupon.index'); // $ artisan route:list
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Coupon::find($id)->delete();

        return back();
    }
}
