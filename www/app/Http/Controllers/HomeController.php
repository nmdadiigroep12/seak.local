<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Models\Product;
use App\Models\Category;
use App\User;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::all();
        $products = Product::orderBy('created_at', 'DESC')->get();
        $bestSoldProducts = Product::orderBy('sold','desc')->take(5)->get();

        $data = [
            'bestSoldProducts' => $bestSoldProducts,
            'products' => $products,
            'categories' => $categories,
        ];

        return view('home',$data);
    }

    public function show($id)
    {
        /**
         * Find the refered category
         */
        $category = Category::findOrFail($id);

        $data = [

            'category' => $category,
        ];

        return view('home',$data);
    }
}
