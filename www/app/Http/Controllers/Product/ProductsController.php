<?php

namespace App\Http\Controllers\Product;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Size;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Input;
use Illuminate\View\View;

class ProductsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index() : View
    {
        $products = Product::with('category','sizes')->orderBy('created_at', 'DESC')->get();

        \Debugbar::info($products);

        $data = [
            'products' => $products,
        ];

        return view('product.products.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() : View
    {
        $categories = Category::pluck('name', 'id');
        $products = new Product();
        $sizes = Size::pluck('sizename','id');

        $data = [
            'categories' => $categories,
            'products' => $products,
            'sizes' => $sizes,
        ];

        return view('product.products.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'title' => 'required|unique:products|max:255',
            'description' => 'required',
            'category' => 'required',
            'sizes' => 'array',
            'image' => 'required'
        ];

        $this->validate($request, $rules);

        $category = Category::find($request->get('category'));
        $products = new Product;
        $products->category()->associate($category);
        $products->title = Input::get('title');
        $products->description = Input::get('description');
        $products->price = Input::get('price');
        $products->stock = Input::get('stock');
        $file = Input::file('image');
        $filename = $file->getClientOriginalName();
        $destinationPath = 'uploaded/';
        $products->image = 'uploaded/'.$filename;
        $uploadSuccess = $file->move($destinationPath,$filename);
        $products->save();

        // linkt de id van het product aan de id van de size
        $products->sizes()->attach($request->get('sizes'));

        return redirect()->route('product.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) : View
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) : View
    {
        $categories = Category::pluck('name', 'id');
        $products = Product::find($id);
        $sizes = Size::pluck('sizename','id');

        $data = [
            'categories' => $categories,
            'products' => $products,
            'sizes' => $sizes,
        ];


        return view('product.products.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = [
            'title' => 'required|max:255',
            'description' => 'required',
            'category' => 'required|int',
            'sizes' => 'array',
        ];


        $products = Product::find($id);


        $products->title = $request->get('title');
        $products->description = $request->get('description');
        $products->price = $request->get('price');
        $products->stock = $request->get('stock');
        $products->category()->associate(Category::find($request->get('category')));
        $products->sizes()->sync($request->get('sizes'));
        $products->save();


        return redirect()->route('product.index'); // $ artisan route:list
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Product::find($id)->delete();

        return back();
    }

}
