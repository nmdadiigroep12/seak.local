<?php
Route::group([
    'middleware' => [
        'cors',
    ],
    'namespace' => 'Api',
    'prefix' => 'api/v1',
], function () {
    $options = [
        'except' => [
            'create',
            'edit',
        ]
    ];
    Debugbar::disable(); // Debugbar is not used for API routes.
    Route::get('categories', 'CategoriesController@categories');
    Route::get('category/{id}', 'CategoriesController@category');
    Route::resource('products', 'ProductsController', $options);
    Route::get('product', 'ProductsController@product');
    Route::resource('sizes', 'SizesController', $options);
    Route::post('login/registreren', 'CostumerController@registreren');
    Route::post('login', 'CostumerController@login');
    Route::post('checkout', 'CheckoutController@checkout');

});