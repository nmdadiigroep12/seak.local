<?php

Route::group(['middleware' => 'web'], function () {


    Route::auth();

    Route::get('/', 'HomeController@index');
    Route::get('/home', 'HomeController@index');
    Route::get('users/show', ['middleware' => 'access', 'uses' => 'HomeController@show']);

    Route::group([
        'namespace' => 'Product'
    ], function () {

        Route::resource('product','ProductsController');
    });

    Route::group ([
        'namespace' => 'Coupon'
    ], function () {

        Route::resource('coupon','CouponController');
    });

    Route::group ([
        'namespace' => 'Review'
    ], function () {

        Route::resource('/review', 'ReviewController');
    });

    Route::group ([
        'namespace' => 'User'
    ], function () {

        Route::resource('/user', 'UserController');
    });

    Route::group ([
        'namespace' => 'Order'
    ], function () {

        Route::resource('/order', 'OrderController');
    });
});