<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Coupon extends Model
{

    protected $fillable = [
        'title',
        'description',
        'percentage',
        'date',
    ];


    public function user() : BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
