<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends Model
{
    use SoftDeletes;

    protected $hidden = [
        'user_id',
        'updated_at',
        'deleted_at',
    ];

    public function user() : BelongsTo
    {
        return $this->BelongsTo(User::class, 'user_id', 'id');
    }
}
