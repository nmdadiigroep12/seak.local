<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Payment extends Model
{
    protected $hidden = [
        'created_at',
        'updated_at',
    ];

    public function user() : HasMany
    {
        return $this->hasMany(User::class);
    }
}
