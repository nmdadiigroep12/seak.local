<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use App\User;

class Review extends Model
{
    protected $fillable = [
        'content', 'rating'
    ];
    /**
     * Many-to-one
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function product() : BelongsTo
    {
        return $this->belongsTo(Product::class);
    }

    /**
     * Many-to-one
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function commentator() : BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
