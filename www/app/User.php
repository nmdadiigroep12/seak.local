<?php

namespace App;

use App\Models\Order;
use App\Models\Coupon;
use App\Models\Review;
use App\Models\Product;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'naam', 'email', 'wachtwoord','voornaam','adres','postcode','stad'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'wachtwoord', 'remember_token',
    ];

    // Relationships
    // =============
    /**
     * One-to-Many
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function coupons()
    {
        return $this->hasMany(Coupon::class);
    }

    /**
     * One-to-Many
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function orders()
    {
        return $this->hasMany(Order::class);
    }

    /**
     * One-to-many
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function reviews()
    {
        return $this->hasMany(Review::class);
    }

    /**
     * One-to-many
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function Favorite()
    {
        return $this->belongsToMany(Product::class, 'product_id', 'id');
    }


}