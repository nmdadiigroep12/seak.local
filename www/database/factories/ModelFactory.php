<?php

use App\User;
use App\Models\Category;
use App\Models\Product;
use App\Models\Review;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(Review::class, function (Faker $faker) : array {
    return [
        CreateProductsTable::FOREIGN_KEY => Product::all()->random()->{CreateProductsTable::PRIMARY_KEY},
        CreateUsersTable::FOREIGN_KEY => User::all()->random()->{CreateUsersTable::PRIMARY_KEY},
        'content' => $faker->paragraph($nbSentences = rand(1, 10)),
        'rating' => $faker->numberBetween($min = 0, $max = 5)
    ];
});

$factory->define(User::class, function (Faker $faker) : array {
    return [
        'naam' => $faker->name,
        'email' => $faker->safeEmail,
        'password' => Hash::make($faker->password(6, 20)),
        'remember_token' => str_random(10),
        'voornaam' => $faker->firstName,
        'naam' => $faker->lastName,
        'adres' => $faker->address,
        'postcode' => $faker->postcode,
        'stad' => $faker->city,
    ];
});
