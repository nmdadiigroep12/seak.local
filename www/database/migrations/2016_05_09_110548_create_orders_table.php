<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    const MODEL = 'order';
    const TABLE = self::MODEL.'s';
    const PRIMARY_KEY = 'id';
    const FOREIGN_KEY = self::MODEL.'_'.self::PRIMARY_KEY;
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(self::TABLE, function (Blueprint $table) {
            // Primary Key
            $table->increments(self::PRIMARY_KEY);

            $table->unsignedInteger(CreatePaymentsTable::FOREIGN_KEY);
            $table->foreign(CreatePaymentsTable::FOREIGN_KEY)
                ->references(CreatePaymentsTable::PRIMARY_KEY)
                ->on(CreatePaymentsTable::TABLE)
                ->onDelete('cascade');

            $table->unsignedInteger(CreateUsersTable::FOREIGN_KEY);
            $table->foreign(CreateUsersTable::FOREIGN_KEY)
                ->references(CreateUsersTable::PRIMARY_KEY)
                ->on(CreateUsersTable::TABLE)
                ->onDelete('cascade');

            // Data
            $table->string('title')->unique();
            $table->longText('description');
            $table->decimal('price');
            $table->double('stock');
            $table->string('image');
            $table->integer('status');


            // Meta Data
            $table->timestamps(); // 'created_at', 'updated_at'
            $table->softDeletes(); // 'deleted_at'
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(self::TABLE);
    }
}
