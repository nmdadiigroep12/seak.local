<?php

use App\Models\Category;
use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Category::create([
            'name' => 'Mannen',
            'description' => 'Dit is alle mannen voor de mannen.',
        ]);

        Category::create([
            'name' => 'Vrouwen',
            'description' => 'Dit is alle mannen voor de vrouwen.',
        ]);

        Category::create([
            'name' => 'Accessoires',
            'description' => 'Dit zijn de Accessoires.',
        ]);
    }
}
