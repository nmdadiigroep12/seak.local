<?php

use App\Models\Product;
use App\Models\Category;
use App\User;
use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Product::create([
            CreateCategoriesTable::FOREIGN_KEY => Category::all()->random()->{CreateCategoriesTable::PRIMARY_KEY},
            'title' => 'LEOTARD WHITE',
            'description' => 'White leotard with printed logo. Made from a cotton/spandex mix. Made in Belgium.',
            'price' => 24.99,
            'stock' => 49,
            'image' => 'uploaded/Woman-Tshirt-03.jpg',
            'sold' => 11,
            'category_id' => '2',
        ]);

        Product::create([
            CreateCategoriesTable::FOREIGN_KEY => Category::all()->random()->{CreateCategoriesTable::PRIMARY_KEY},
            'title' => 'BOX TEE WHITE',
            'description' => 'Oversized white boxy fit tee with printed logo. Made from 65% polyester and 35% viscose.',
            'price' => 24.99,
            'stock' => 75,
            'image' => 'uploaded/Woman-Tshirt-01.jpg',
            'sold' => 19,
            'category_id' => '2',
        ]);

        Product::create([
            CreateCategoriesTable::FOREIGN_KEY => Category::all()->random()->{CreateCategoriesTable::PRIMARY_KEY},
            'title' => 'BOX TEE BLACK',
            'description' => 'Oversized boxy fit top with logo. Made from 65% polyester and 35% viscose.',
            'price' => 24.99,
            'stock' => 53,
            'image' => 'uploaded/Woman-Tshirt-02.jpg',
            'sold' => 14,
            'category_id' => '2',
        ]);

        Product::create([
            CreateCategoriesTable::FOREIGN_KEY => Category::all()->random()->{CreateCategoriesTable::PRIMARY_KEY},
            'title' => 'GREY FKD BEANIE',
            'description' => '100% Soft touch acrylic with embroided logo. One size, fits all. This is sporty looking, who loves sports? Just me? SPORT!',
            'price' => 15.99,
            'stock' => 94,
            'image' => 'uploaded/Acc-Muts-01.jpg',
            'sold' => 5,
            'category_id' => '3',
        ]);

        Product::create([
            CreateCategoriesTable::FOREIGN_KEY => Category::all()->random()->{CreateCategoriesTable::PRIMARY_KEY},
            'title' => 'GREY FKD SWEATSHIRT',
            'description' => 'Grey sweat with standard fit and blue printed FKD-logo. Made from a sports mesh material. 100% Cotton.',
            'price' => 44.99,
            'stock' => 103,
            'image' => 'uploaded/Man-Sweat-01.jpg',
            'sold' => 9,
            'category_id' => '1',
        ]);

        Product::create([
            CreateCategoriesTable::FOREIGN_KEY => Category::all()->random()->{CreateCategoriesTable::PRIMARY_KEY},
            'title' => 'BLACK FKD SWEATSHIRT',
            'description' => 'Black sweat with standard fit and white printed FKD-logo\'s. Made from a sports mesh material. 100% Cotton.',
            'price' => 44.99,
            'stock' => 130,
            'image' => 'uploaded/Man-Sweat-02.jpg',
            'sold' => 1,
            'category_id' => '1',
        ]);

        Product::create([
            CreateCategoriesTable::FOREIGN_KEY => Category::all()->random()->{CreateCategoriesTable::PRIMARY_KEY},
            'title' => 'BLACK LIFE T-SHIRT',
            'description' => 'Black t-shirt with white text. Made from 100% Cotton.',
            'price' => 24.99,
            'stock' => 78,
            'image' => 'uploaded/Man-Tshirt-01.jpg',
            'sold' => 7,
            'category_id' => '1',
        ]);
    }
}
