<?php

use App\Models\Size;
use Illuminate\Database\Seeder;

class SizesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Size::create([
            'sizename' => 'XS',
        ]);

        Size::create([
            'sizename' => 'S',
        ]);

        Size::create([
            'sizename' => 'M',
        ]);

        Size::create([
            'sizename' => 'L',
        ]);

        Size::create([
            'sizename' => 'XL',
        ]);
    }
}
