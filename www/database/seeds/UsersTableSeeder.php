<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'email' => 'leevh1995@hotmail.com',
            'password' => Hash::make('nmdad1'),
            'voornaam' => 'Lee',
            'naam' => 'Van Hecke',
            'adres' => 'Gentdam',
            'postcode' => 9160,
            'stad' => 'Lokeren',
        ]);

        User::create([
            'email' => 'senne_van@hotmail.com',
            'password' => Hash::make('nmdad2'),
            'voornaam' => 'Senne',
            'naam' => 'Vanfleteren',
            'adres' => 'Rode Kruislaan 27',
            'postcode' => 8630,
            'stad' => 'Veurne',
        ]);

        factory(User::class, DatabaseSeeder::AMOUNT['DEFAULT'])->create();
    }
}
