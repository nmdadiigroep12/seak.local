@extends('layouts.app')

@section('content')
<div id="wrapper">
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">{{ $coupons->title }}</h1>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                {!! Form::model($coupons, [
                    'files' => true,
                    'method' => 'put',
                    'route' => ['coupon.update',$coupons->id],
                ]) !!}

                <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                    {!! Form::label($name = 'title',$value = 'Naam:', $options = ['class' => 'control-label',]) !!}
                    {!! Form::text($name = 'title',$value = null, $options = ['class' => 'form-control','placeholder' => 'De naam voor de coupon.',]) !!}
                </div>

                <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                    {!! Form::label($name = 'description',$value = 'Beschrijving:', $options = ['class' => 'control-label',]) !!}
                    {!! Form::textarea($name = 'description',$value = null , $options = ['class' => 'form-control','placeholder' => 'Een beschrijving van de coupon.',]) !!}
                </div>

                <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                    {!! Form::label($name = 'percentage',$value = 'Percentage:', $options = ['class' => 'control-label',]) !!}
                    {!! Form::text($name = 'percentage',$value = null , $options = ['class' => 'form-control','placeholder' => 'De korting op het product.',]) !!}
                </div>

                <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                    {!! Form::label($name = 'date',$value = 'Datum:', $options = ['class' => 'control-label',]) !!}
                    {!! Form::date($name = 'date',$value = null , $options = ['class' => 'form-control',]) !!}
                </div>

                <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                    {!! Form::label($name = 'code', $value = 'Code:', $options = ['class' => 'control-label', ]) !!}
                    {!! Form::text($name = 'code', $value = null , $options = ['class' => 'form-control','placeholder' => 'De couponcode.',]) !!}
                </div>

                {!! Form::submit($value = 'Save', $options = ['class' => 'btn btn-primary',]) !!}
                {!! link_to_route($name = 'coupon.index', $title = 'Cancel', $parameters = null, $attributes = ['class' => 'btn btn-default',]) !!}
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
@endsection
