@extends('layouts.app')

@section('content')
<div id="wrapper">
    <div id="page-wrapper">
        <div class="row">
            <div class="col-xs-12">
                <div class="row">
                    <h1 class="col-xs-6 page-header">Coupons</h1>
                    <div class="col-xs-6 page-header" style="margin-top: 32px">
                        <a role="link" class="btn btn-info pull-right"  href="{{ route('coupon.create') }}" >Nieuw Coupon</a>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                @forelse($coupons as $coupon)
                    <div class="col-lg-4 thumbnail">
                        <div class="wit-klein">
                            <h2 class="text-center">{{ $coupon->title }}</h2>
                            <p class="text-center">{{ $coupon->description }}</p>
                            <p class="text-center">{{ $coupon->percentage }}%</p>
                            <p class="text-center">{{ $coupon->date }}</p>
                            <p class="text-center">{{ $coupon->code }}</p>

                            <div class="row">
                                <div class="col-xs-2 col-xs-push-4 text-center">
                                    <a class="btn btn-info modal-button text-center" href="{{ route('coupon.edit', $coupon->id) }}" data-toggle="modal" data-placement="top" title="Edit">
                                        <i class="fa fa-pencil"></i>
                                    </a>
                                </div>
                                {!! Form::open(['method'=>'DELETE','route'=>['coupon.destroy', $coupon->id], 'class' => 'col-xs-2 col-xs-push-4 text-center']) !!}
                                <button type="submit" class="btn btn-danger" data-toggle="tooltip" data-placement="top" title="Delete">
                                    <i class="fa fa-trash-o"></i>
                                </button>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                @empty
                    <div class="col-md-12 wit">
                        <h3 class="text-center">Voeg een coupon toe om te starten</h3>
                    </div>
                @endforelse
            </div>
        </div>
    </div>
</div>
@endsection