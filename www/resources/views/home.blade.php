@extends('layouts.app')

@section('content')

<div id="wrapper">
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Dashboard</h1>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <i class="fa fa-bar-chart-o fa-fw"></i> Verkoop
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <div id="myfirstchart" style="height: 250px;"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <i class="fa fa-archive fa-fw"></i> Stock
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            @foreach ($categories as $category)
                            <div class="col-xs-12 col-md-4">
                                @if(count($category->products) > 0)
                                <h2 class="text-center">{{$category->name}}</h2>
                                <table class="table table-bordered">
                                    <tbody>
                                        <tr>
                                            <th class="bg-primary">Kledingstuk</th>
                                            <th class="bg-primary">Stock</th>
                                        </tr>
                                        @foreach ($category->products as $product)
                                        <tr>
                                            <td>{{$product->title}}</td>
                                            <td>{{$product->stock}}</td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                @else
                                <p>
                                    Er zijn nog geen producten in deze categorie.
                                </p>
                                @endif
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <i class="fa fa-clock-o fa-fw"></i> Best verkochte kledingstukken
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-xs-12">
                                <table class="table table-bordered">
                                    <tbody>
                                    <tr>
                                        <th class="bg-primary">Kledingstuk</th>
                                        <th class="bg-primary">Verkocht</th>
                                    </tr>
                                    @forelse ($bestSoldProducts as $bestSoldProduct)
                                    <tr>
                                        <td>{{$bestSoldProduct->title}}</td>
                                        <td>{{$bestSoldProduct->sold}}</td>
                                    </tr>
                                    @empty
                                        <p>Er is geen data beschikbaar</p>
                                    @endforelse
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection