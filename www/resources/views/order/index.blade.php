@extends('layouts.app')

@section('content')<div id="wrapper">
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Orders</h1>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                @if ($orders == null || count($orders) < 1)
                    <div class="col-lg-12">
                        <h3 class="text-center">Er zijn geen orders beschikbaar.</h3>
                    </div>
                @else
                    <table class="table">
                        <tbody>
                        <tr>
                            <th>Status</th>
                            <th>Titel</th>
                            <th>Prijs</th>
                            <th>Klant</th>
                            <th></th>
                            <th></th>
                        </tr>
                        @foreach ($orders as $order)
                            <tr>
                                <td>
                                    @if($order->status == 0)
                                        <span class="label label-danger">nieuw</span>
                                    @elseif($order->status == 1)
                                        <span class="label label-warning">verpakt</span>
                                    @elseif($order->status == 2)
                                        <span class="label label-primary">onderweg</span>
                                    @elseif($order->status == 3)
                                        <span class="label label-success">geleverd</span>
                                    @endif
                                </td>
                                <td>{{$order->title}}</td>
                                <td>{{$order->price}}</td>
                                <td>{{$order->costumer->voornaam}} {{$order->costumer->naam}}</td>
                                <td>
                                    {!! Form::open(['method' => 'PUT','route' => ['purchase.status', $order->id]]) !!}
                                    {{ Form::hidden( 'status', ($order->status+1) ) }}
                                    @if($order->status == 0)
                                        {!! Form::submit('Opgepikt', ['class' => 'btn btn-primary form-control']) !!}
                                    @elseif($order->status == 1)
                                        {!! Form::submit('Verzonden', ['class' => 'btn btn-primary form-control']) !!}
                                    @elseif($order->status == 2)
                                        {!! Form::submit('Geleverd', ['class' => 'btn btn-primary form-control']) !!}
                                    @endif
                                    {!! Form::close() !!}
                                </td>
                                <td>
                                    {!! Form::open(['method' => 'DELETE','route' => ['purchase.destroy', $order->id], 'class' => 'form-delete']) !!}
                                    <a class="btn btn-danger modal-button" data-product-id="{{ $order->id }}" data-toggle="modal" href="#modal-confirm"><i class="fa fa-trash-o"></i></a>
                                    {!! Form::close() !!}
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                @endif
            </div>
        </div>
    </div>
</div>
@endsection
