@extends('layouts.app')

@section('content')
<div id="wrapper">
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Nieuw Product</h1>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                @if(null !== Session::get('error'))
                    <div class="alert alert-warning">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        <strong>Er is iet fout!</strong> {{ Session::get('error') }}
                    </div>
                @endif
                {!! Form::model($products, [
                    //array('url' => 'product/create'),
                    'files' => true,
                    'route' => ['product.store'],
                    'role' => 'form'
                ]) !!}

                <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                    {!! Form::label($name = 'title',$value = 'Naam:', $options = ['class' => 'control-label',]) !!}
                    {!! Form::text($name = 'title',$value = null, $options = ['class' => 'form-control','placeholder' => 'De naam voor het product.',]) !!}
                </div>

                <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                    {!! Form::label($name = 'description',$value = 'Beschrijving:', $options = ['class' => 'control-label',]) !!}
                    {!! Form::textarea($name = 'description',$value = null , $options = ['class' => 'form-control','placeholder' => 'Een beschrijving van het product.',]) !!}
                </div>

                <div class="form-group">
                    {!! Form::label($name = 'category', $value = 'Category', $options = ['class' => 'control-label',]) !!}
                    {!! Form::select($name = 'category', $value = $categories, $selected = null, $options = ['class' => 'form-control','placeholder' => 'Kies een categorie.',]) !!}
                </div>

                <div class="form-group">
                    {!! Form::label($name = 'size', $value = 'sizes', $options = ['class' => 'control-label',]) !!}
                    {!! Form::select($name = 'sizes', $value = $sizes, $selected = null, $options = ['class' => 'form-control','multiple' => 'mutiple','name' => 'sizes[]',]) !!}
                </div>

                <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                    {!! Form::label($name = 'price',$value = 'Prijs:', $options = ['class' => 'control-label',]) !!}
                    {!! Form::text($name = 'price',$value = null , $options = ['class' => 'form-control','placeholder' => 'De prijs van het product.',]) !!}
                </div>

                <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                    {!! Form::label($name = 'stock',$value = 'Stock:', $options = ['class' => 'control-label',]) !!}
                    {!! Form::text($name = 'stock',$value = null , $options = ['class' => 'form-control','placeholder' => 'Stock instellen van het product.',]) !!}
                </div>

                <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                    {!! Form::label($name = 'image', $value = 'Afbeelding:', $options = ['class' => 'control-label',]) !!}
                    {!! Form::file($name = 'image', $options = ['accept' => '.jpg,.jpeg,.png,.svg,image/*',]) !!}
                    <p class="help-block">Upload a picture</p>
                </div>

                <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                    {!! Form::label($name = 'alternate_text', $value = 'Voettekst:', $options = ['class' => 'control-label', ]) !!}
                    {!! Form::text($name = 'alternate_text', $value = null , $options = ['class' => 'form-control','placeholder' => 'De tekst voor je afbeelding.',]) !!}
                </div>

                {!! Form::submit($value = 'Save', $options = ['class' => 'btn btn-primary',]) !!}
                {!! link_to_route($name = 'product.index', $title = 'Cancel', $parameters = null, $attributes = ['class' => 'btn btn-default',]) !!}
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
@endsection
