@extends('layouts.app')

@section('content')
<div id="wrapper">
    <div id="page-wrapper">
        <div class="row">
            <div class="col-xs-12">
                <div class="row">
                    <h1 class="col-xs-6 page-header">Producten</h1>
                    <div class="col-xs-6 page-header" style="margin-top: 32px">
                        <a role="link" class="btn btn-info pull-right"  href="{{ route('product.create') }}" >Nieuw Product</a>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12">
                <div class="row products">
                    @forelse($products as $product)
                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4">
                            <div class="thumbnail">
                                <h2 class="col-xs-12">{{ $product->title }}</h2>
                                <p class="col-xs-12">{{ $product->description }}</p>
                                <img src="{{ asset('/'.$product->image) }}" class="center-block">
                                <div class="col-xs-12">
                                    <table>
                                        <tr class="row">
                                            <th class="text-center col-xs-4">Prijs</th>
                                            <th class="text-center col-xs-4">Stock</th>
                                            <th class="text-center col-xs-4">Categorie</th>
                                        </tr>
                                        <tr class="row">
                                            <td class="text-center col-xs-4">€{{ $product->price }}</td>
                                            <td class="text-center col-xs-4">{{ $product->stock }}</td>
                                            <td class="text-center col-xs-4">{{ $product->category->name }}</td>
                                        </tr>
                                    </table>
                                </div>

                                <div class="row">
                                    <div class="col-xs-2 col-xs-push-4 text-center">
                                        <a class="btn btn-info modal-button text-center" href="{{ route('product.edit', $product->id) }}" data-toggle="modal" data-placement="top" title="Edit">
                                            <i class="fa fa-pencil"></i>
                                        </a>
                                    </div>
                                    {!! Form::open(['method'=>'DELETE','route'=>['product.destroy', $product->id], 'class' => 'col-xs-2 col-xs-push-4 text-center']) !!}
                                    <button type="submit" class="btn btn-danger" data-toggle="tooltip" data-placement="top" title="Delete">
                                        <i class="fa fa-trash-o"></i>
                                    </button>
                                    {!! Form::close() !!}
                                </div>

                            </div>
                        </div>
                    @empty
                        <div class="col-lg-12">
                            <h3 class="text-center">Voeg een product toe om te starten</h3>
                        </div>
                    @endforelse
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
