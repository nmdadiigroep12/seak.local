@extends('layouts.app')

@section('content')
<div id="wrapper">
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Reviews</h1>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                @if ($reviews == null || count($reviews) < 1)
                    <div class="col-lg-12">
                        <h1 class="text-center">Er zijn nog geen reviews beschikbaar.</h1>
                    </div>
                @else
                    <table class="table">
                        <tbody>
                        <tr>
                            <th>Klant</th>
                            <th>Product</th>
                            <th>Content</th>
                            <th>Rating</th>
                            <th>Actions</th>
                        </tr>
                        @foreach ($reviews as $review)
                            <tr>
                                <td>{{$review->commentator->voornaam}} {{$review->commentator->naam}}</td>
                                <td>{{$review->product->title}}</td>
                                <td>{{$review->content}}</td>
                                <td>{{$review->rating}}</td>
                                <td>
                                    {!! Form::open(['method' => 'DELETE','route' => ['review.destroy', $review->id], 'class' => 'form-delete']) !!}
                                    <button type="submit" class="btn btn-danger" data-toggle="tooltip" data-placement="top" title="Delete">
                                        <i class="fa fa-trash-o"></i>
                                    </button>
                                    {!! Form::close() !!}
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                @endif
            </div>
        </div>
    </div>
</div>
@endsection

@section('footer')
        <!-- jQuery -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/metisMenu/2.5.2/metisMenu.min.js"></script>

<!-- Morris Charts JavaScript -->
<script src="//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.min.js"></script>
<script src="http://cdn.oesmith.co.uk/morris-0.4.3.min.js"></script>
<script src="../js/morris-data.js"></script>
@endsection