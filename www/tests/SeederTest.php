<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class SeederTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->seeInDatabase(CreateUsersTable::TABLE, [
            'email' => 'info@seak.be',
            'name' => 'LeeVanHecke',
            'given_name' => 'Lee',
            'family_name' => 'Van Hecke',
        ]);
    }
}